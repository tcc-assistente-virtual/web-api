  (ns test_api.core
  (:require [ring.adapter.jetty :refer [run-jetty]]
            [test_api.controllers.usercontroller :refer [user-routes]]
            [test_api.controllers.trafficlightcontroller :refer [trafficlight-routes]]
            [test_api.controllers.locationcontroller :refer [location-routes]]
            [compojure.api.sweet :refer [api routes]]
            [test_api.db :refer [db]]
            [test_api.sql :as sql]
            [clojure.pprint :as pprint]
            [ring.middleware.cors :refer [wrap-cors]])
  (:gen-class))



(def swagger-config
  {:ui "/swagger"
   :spec "/swagger.json"
   :options {:ui {:validatorUrl nil}
             :data {:info {:version "1.0.0", :title "Restful CRUD API"}}}})

(def app (api {:swagger swagger-config} (apply routes user-routes trafficlight-routes location-routes)))
				 ;;(wrap-cors (apply routes user-routes)  :access-control-allow-origin [#".*"] 
				 ;;										:access-control-allow-methods [:get :put :post :delete])))

(defn -main
  [& args]
  (run-jetty app {:port 3000})
  (println "Hello, World!")
  )