(ns test_api.services.location
  (:require [test_api.services.distance :refer [distance-km]]
))

(defn normalize-location-name [name]
  (clojure.string/trim (clojure.string/lower-case (str "%" name "%")))
  )


(def min-distance-km 0.6);;Min distance in KM to return a value

;;Get the closest points.
(defn get-points-of-interest[locations latitude longitude]
	(let [resultCalculated 
                  (for [x locations] 
                        (assoc x :distance (distance-km (:latitude x) (:longitude x) (Float/parseFloat latitude) (Float/parseFloat longitude)))
                  )]
 	(sort-by :distance (filter #(<= (:distance %) min-distance-km) resultCalculated))
))