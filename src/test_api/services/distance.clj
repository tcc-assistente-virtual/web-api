(ns test_api.services.distance)

;; Convert degrees to radians (degrees * (PI / 180)).
(defn degrees-to-radians [degrees]
  (* degrees (/ Math/PI 180)))

;; Convert radians to degrees
(defn radians-to-degrees [radians]
  (/ (* radians 180) Math/PI))

;; Distance in kms between two coordinates.
(defn distance-km [latitude1 longitude1 latitude2 longitude2]
  (let [earthRadius 6371
        dLat (degrees-to-radians (- latitude2 latitude1))
        dLon (degrees-to-radians (- longitude2 longitude1))
        latitude1Radians (degrees-to-radians latitude1)
        latitude2Radians (degrees-to-radians latitude2)
        a (+
           (* (Math/sin (/ dLat 2)) (Math/sin (/ dLat 2)))
           (* (* (* (Math/sin (/ dLon 2)) (Math/sin (/ dLon 2))) (Math/cos latitude1Radians)) (Math/cos latitude2Radians)))
        c (* 2 (Math/atan2 (Math/sqrt a) (Math/sqrt (- 1 a))))]
   (* earthRadius c)))

;; Calculates the bearing given two points
(defn bearing [latitude1 longitude1 latitude2 longitude2]
  (let [ startLat (degrees-to-radians latitude1)
         startLng (degrees-to-radians longitude1)
         destinationLat (degrees-to-radians latitude2)
         destinationLng (degrees-to-radians longitude2)

         y (* (Math/sin (- destinationLng startLng)) (Math/cos destinationLat))
         x (- (* (Math/cos startLat) (Math/sin destinationLat)) 
              (* (Math/sin startLat) (Math/cos destinationLat) (Math/cos (- destinationLng startLng))))

        brng (Math/atan2 y x)
        brngDegrees (radians-to-degrees brng)]
        
  (mod (+ brngDegrees 360) 360)))

;;Calculates the heading(h) to a point, considering the heading of the device.
(defn relative-bearing [latitude1 longitude1 latitude2 longitude2 heading]
  (let [ brng (bearing latitude1 longitude1 latitude2 longitude2)
         h (* (- heading brng) -1)]
   (mod (+ h 360) 360)))

;;Verify if the calculated relative bearing is pointing in the direction of the point
(defn bearing-front [relativeBearing]
	(println "true " relativeBearing) 
  (cond
       (or (>= relativeBearing 310) (<= relativeBearing 50)) true
       :else false))