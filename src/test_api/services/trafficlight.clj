(ns test_api.services.trafficlight
	(:require [test_api.services.distance :refer [distance-km relative-bearing bearing-front]]
))

(def min-distance-km 0.5);;Min distance in KM to return a value



;;In a list of trafficlights gets the one that is closer to a location.
(defn get-min-distance[trafficlights latitude longitude heading]
	(let [resultCalculated 
                  (for [x trafficlights] 
                        (assoc x :distance (distance-km (:latitude x) (:longitude x) (Float/parseFloat latitude) (Float/parseFloat longitude))
                                 :relativeBearing (relative-bearing (Float/parseFloat latitude) (Float/parseFloat longitude) (:latitude x) (:longitude x)  (Float/parseFloat heading)))
                  )
          closest (apply min-key :distance resultCalculated)]
 	(cond
       (and (<= (:distance closest) min-distance-km) (bearing-front (:relativeBearing closest))) closest 
       :else nil)))