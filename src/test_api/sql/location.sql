-- :name location-check :? :*
-- :doc seleciona uma location 
SELECT * 
	FROM location
	WHERE locationid = CAST (:id AS INTEGER)


-- :name location-all
-- :doc seleciona todos
SELECT * 
	FROM location

-- :name location-search :? :*
-- :doc seleciona uma location 
SELECT * 
	FROM location
	WHERE locationname LIKE :locationname

-- :name location-type-search :? :*
-- :doc seleciona uma location 
SELECT loc.*, typ.locationtypename
FROM location loc
	INNER JOIN relationlocationtype rlt ON loc.locationid = rlt.locationid
	INNER JOIN locationtype typ ON rlt.locationtypeid = typ.locationtypeid 
WHERE typ.locationtypename LIKE :locationtype