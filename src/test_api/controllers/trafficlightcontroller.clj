(ns test_api.controllers.trafficlightcontroller
  (:require [schema.core :as s]
            [test_api.models.trafficlight :refer [Trafficlight]]
            [test_api.services.trafficlight :refer [get-min-distance]]
            [test_api.db :refer [db]]
            [test_api.sql :as sql]
            [ring.util.http-response :refer [ok not-found created]]
            [compojure.api.sweet :refer [GET POST PUT DELETE]]
            [test_api.db :refer [db]]
            [test_api.sql :as sql]
            [clojure.data.json :as json]))




(def trafficlight-routes
  [
  	;;Returns the traffic light closer to the coordinates.
   (GET ["/trafficlight/:latitude/:longitude/:heading"] [latitude longitude heading]
     (json/write-str (get-min-distance (sql/trafficlight-all db) latitude longitude heading)))

   ;;trafficlights
   (GET ["/trafficlight/:id"] [id]
     ;;(json/write-str (sql/trafficlight-check db {:id id}))
     (json/write-str (sql/trafficlight-check db {:id id})))
   ])


