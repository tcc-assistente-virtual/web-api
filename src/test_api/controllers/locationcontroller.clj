(ns test_api.controllers.locationcontroller
  (:require [schema.core :as s]
            [test_api.models.location :refer [Location]]
            [test_api.services.location :refer [normalize-location-name get-points-of-interest]]
            [test_api.db :refer [db]]
            [test_api.sql :as sql]
            [ring.util.http-response :refer [ok not-found created]]
            [compojure.api.sweet :refer [GET POST PUT DELETE]]
            [test_api.db :refer [db]]
            [test_api.sql :as sql]
            [clojure.data.json :as json]))




(def location-routes
  [
   ;;location
   (GET ["/location/:id"] [id]
     (json/write-str (sql/location-check db {:id id})))

  ;;finds points of interest in a specific radius
   (GET ["/location/points-of-interest/:latitude/:longitude"] [latitude longitude]
     (json/write-str (get-points-of-interest (sql/location-all db) latitude longitude)))
    
   ;;finds a specifc type of poits of interest
   (GET ["/location/points-of-interest-by-type/:latitude/:longitude/:type"] [latitude longitude type]
     (json/write-str (get-points-of-interest (sql/location-type-search db {:locationtype (normalize-location-name type)}) latitude longitude)))
   
   ;;finds a location based on the name
   (GET ["/location/search/:locationname"] [locationname]
     (json/write-str (sql/location-search db {:locationname (normalize-location-name locationname)})))
   ])


