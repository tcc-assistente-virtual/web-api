(ns test_api.db)

(def db
 {:classname "org.postgresql.Driver"
  :subprotocol "postgresql"
  :subname "//127.0.0.1:5432/tcc"
  :user "postgres"
  :password "password"
  })